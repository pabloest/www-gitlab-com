---
layout: handbook-page-toc
title: "Enterprise Sales Playbook"
description: "The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance.

| Educate & Engage | Facilitate the Opportunity | Deal Closure | Retain and Expand |
| ------ | ------ | ------ | ------ |
| * [Prospecting](/handbook/sales/prospecting/)<br> * Lead w/ Insights | * [Discovery](/handbook/sales/playbook/discovery/)<br> * [Command Plan](/handbook/sales/command-of-the-message/command-plan/)<br> * [Proof of Value](/handbook/customer-success/solutions-architects/tools-and-resources/pov/)<br> -----> [Demos](/handbook/customer-success/solutions-architects/demonstrations/)<br> * [The Business Case](/handbook/sales/command-of-the-message/metrics/) <br>- [Champions](/handbook/sales/meddppicc/#champion) | * [Mutual Close Plan](/handbook/sales/mutual-close-plan)<br> * Customer-Centric Proposal<br> * [Negotiate to Close](/handbook/sales/negotiate-to-close/)<br> * [Order Processing](/handbook/sales/field-operations/order-processing/)<br> -----> [Quote Configuration](/handbook/sales/field-operations/order-processing/#quote-configuration)<br> -----> [How to Send an Order Form](/handbook/sales/field-operations/order-processing/#how-to-send-an-order-form-to-a-customer)<br> -----> [Submit Opp for Booking](/handbook/sales/field-operations/order-processing/#submit-an-opportunity-for-booking)| * [Account Transition](/handbook/customer-success/pre-sales-post-sales-transition/)<br> * [Customer Onboarding](/handbook/customer-success/tam/onboarding/) <br> * [Success Plans](/handbook/customer-success/tam/success-plans/) <br> * [Account Planning](/handbook/sales/account-planning/)<br> * [Executive Business Reviews](/handbook/customer-success/tam/ebr/)<br> * [Customer Health Assessment](/handbook/customer-success/tam/health-score-triage/)<br> * [Renewals](/handbook/customer-success/tam/renewals/) |

_Note: A Commercial Sales Playbook will be developed as a component of the [Commercial Sales Handbook](/handbook/sales/commercial/) (timing TBD)._

## Featured Podcasts

- [Lessons From a Sales Veteran](https://theaudible-readypodcast.sounder.fm/episode/lessons-from-a-sales-veteran-w-frank-azzolino) (37 minutes)
- [Force Management's Most Frequently Asked Questions from Salespeople](https://theaudible-readypodcast.sounder.fm/episode/27-our-most-faqs-from-salespeople-w-john-kaplan) (14 minutes)
- [Competing Against Doing Nothing](https://theaudible-readypodcast.sounder.fm/episode/35-competing-against-do-nothing-w-john-kaplan) (8.5 minutes)
- [Leveraging the Technical Mind](https://theaudible-readypodcast.sounder.fm/episode/leveraging-the-technical-mind-w-john-kaplan) (15 minutes)
- [The Art of the Demo](https://theaudible-readypodcast.sounder.fm/episode/the-art-of-the-demo-w-john-kaplan) (19 minutes) 
